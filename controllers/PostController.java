package com.zuitt.wdc044.controllers;

import com.zuitt.wdc044.models.Post;
import com.zuitt.wdc044.services.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;



@RestController
@CrossOrigin
public class PostController {
    @Autowired
    PostService postService;

    // create a new post
    @RequestMapping(value = "/posts", method = RequestMethod.POST)
    // "ResponseEntity" represents the whole HTTP response: status code, headers, and body
    public ResponseEntity<Object> createPost(@RequestHeader(value = "Authorization") String stringToken, @RequestBody Post post){
        //We can access the "postService" methods and pass the following arguments:
            // stringToken of the current session will be retrieved in the request headers.
            // a "post" object will be instantiated upon receiving the request body, this will follow the properties set in the model.
                // note: the "key" name of the request from postman should be similar to the property names set in the model.
        postService.createPost(stringToken, post);
        return new ResponseEntity<>("Post created successfully", HttpStatus.CREATED);
    }

    // Get all posts
    //s04 Activity:
    // Add a PostController method to retrieve all the posts from the database. This method should respond to a GET request at the /posts endpoint, and return a new response entity that calls the getPosts() method from postService.java. No arguments will be needed for this method.
    @RequestMapping(value = "/posts", method = RequestMethod.GET)
    public ResponseEntity<Object> getPosts(){
        return new ResponseEntity<>(postService.getPosts(),HttpStatus.OK);
    }
    //Edit a post
    @RequestMapping(value = "/posts/{postId}", method = RequestMethod.PUT)
    public ResponseEntity<Object> updatePost(@PathVariable Long postId, @RequestHeader(value = "Authorization") String stringToken, @RequestBody Post post){
        return postService.updatePost(postId, stringToken, post);
    }

    //Delete a post
    @RequestMapping(value = "/posts/{postId}", method = RequestMethod.DELETE)
    public ResponseEntity<Object> deletePost(@PathVariable Long postId, @RequestHeader(value = "Authorization") String stringToken){
        return postService.deletePost(postId, stringToken);
    }

    // Activity for s05 Instruction:
        // 1. Create a feature for retrieving a list of all a user's posts.
        // 2. This feature needs a controller method run by a GET request to the /myPosts endpoint.
        // 3. It should call another method in the service implementation, and passes a string token from the user's JWT.

        // Hint: UserRepository.findByUserName and jwtToken.getUsernameFromToken can be used to accomplish this, similar to how it used in deleting and updating a user's posts.

    @RequestMapping(value = "/myPosts/{userId}", method = RequestMethod.GET)
    public ResponseEntity<Object> getMyPosts(@PathVariable Long postId, @RequestHeader(value = "Authorization") String stringToken){
        return (ResponseEntity<Object>) postService.getPosts();}
}
